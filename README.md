# mk3

![](mk3-logo.svg)

mk3 or make tree is a binary, currently distributed as ruby gem which helps you create directory and file structure.

# Installation

## As Ruby gem

If you have [Ruby](https://ruby-lang.org) installed, all you need to do is this:

```
$ gem install mk3
```

## Using brew

I did not test this one, but I hope it works. Definitely won't make your computer go up in flames I suppose.

First install this one:

```
$ brew install brew-gem
```

Then run this:

```
$ brew gem install mk3
```


# Usage

```
$ mk3

Enter directory structure: (Press Ctrl+D or Ctrl+Z to finish)

directory/
  sub_directory/
    some_file.md
  another_sub_directory/
```

When `Ctrl+D` or `Ctrl+Z` is pressed, the directory and files will be created.

**Make sure the nesting is two spaces deep.**

Or you can enter the file structure in a file and feed it to mk3 as shown:

```
$ mk3 dir_and_files.txt
```

mk3 will read `dir_and_files.txt` and will create the structure for you.


# License

Released under [GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html) or later.


# Donate

If you feel like donating, you can do so here at [PayPal](https://paypal.me/mindaslab).
