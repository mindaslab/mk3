Gem::Specification.new do |s|
  s.name        = "mk3"
  s.version     = "0.0.0"
  s.summary     = "make directory and file tree"
  # s.description = "make directory and file tree"
  s.authors     = ["Karthikeyan A K"]
  s.email       = "mindaslab@protonmail.com"
  s.files       = ["lib/line_processor.rb"]
  s.executables << 'mk3'
  s.homepage    = "https://gitlab.com/mindaslab/mk3"
  s.license     = "GPL-3.0-or-later"
  s.add_runtime_dependency 'tty-prompt', '~> 0.23'
end
