require 'fileutils'
SPACE_LENGTH = 2

module Mk3
  def self.create_direcory? line
    line.match /\/$/
  end

  def self.touch_file? line
      ! create_direcory? line
  end

  def self.create_directory line
    FileUtils.mkdir_p line.strip
  end

  def self.touch_file line
    FileUtils.touch(line.strip) unless line.strip.empty?
  end

  def self.level line
    match = line.match(/^\s+/)
    match[0].size / SPACE_LENGTH if match
  end

  def self.lines_processor lines
    array = []

    for line in lines
      line_level = level(line).to_i
      array[line_level] = line.strip
      array = array[..line_level]
      string = array.join

      unless line.strip.empty?
        create_directory(string) if create_direcory?(string)
        touch_file(string) if touch_file?(string)
      end
    end
  end
end
